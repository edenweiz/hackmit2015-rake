var myoLeftDown = false;
var myoRightDown = false;

function setup() {
    Myo.on('connected', function() {
        console.log("connected successfully!!");
        /*console.log(Myo.myos);
        console.log(Myo.myos.length + " myos connected.");
        for (var i = 0; i < Myo.myos.length; i++) {
        	console.log("checking " + Myo.myos[i].arm);
            if (Myo.myos[i].arm == "right") {
                console.log("we got a rightie");
            }
        }*/
        Myo.setLockingPolicy("none");
    });

    Myo.on('pose', function(pose) {
        console.log('Hello Myo!' + pose);
        if (String(pose) == "wave_out"){
            myoRightDown = true;
        }
    });

    Myo.on('pose_off', function(pose){
        if (String(pose) == "wave_out"){
            myoRightDown = false;
        }
    });

    Myo.connect();
}